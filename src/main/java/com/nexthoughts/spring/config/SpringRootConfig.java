package com.nexthoughts.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.nexthoughts.spring.services")
public class SpringRootConfig {
//saass
}
